Deep Six CBD is Philadelphia, PA's premier online and in-store retailer of CBD products. Shop for CBD online at one of our six locations around Philadelphia for CBD edibles, tinctures, topicals, vape juice & even CBD for pets.

Address: 500 W Germantown Pike, #2285, Plymouth Meeting, PA 19462, USA

Phone: 610-897-8740

Website: https://www.deepsixcbd.com
